#include <stdio.h>
#include <math.h>
#include <stdlib.h>

/* Compile with 
* gcc TwoDimensionalRender.c -lm -o TwoDimensionalRender
*
* run with
* ./TwoDimensionalRender > image.ppm
*/

int main(){
  const int mx  = 1000;   // monitor x location
  const int myt = 1000;   // monitor y location top
  const int myb = 3000;   // monitor y location bottom
  const int mzt = 1000;   // monitor z location top
  const int mzb = 3000;   // montor z location bottom
  const int ex  = 0;      // eye x location
  const int ey  = 2000;   // eye y location
  const int ez  = 2000;   // eye z location
  const int lcx = 5000;   // light center x location
  const int lcy = 2000;   // light center y location
  const int lcz = 2000;   // light center z location
  const int lr  = 500;    // light radius
  const int maxval=250;   // maximum rgb color value
  int my;                 // monitor y location
  int mz;                 // monitor z location
  // array to store output, 1 if real solution, 0 otherwise
  int *real_solution;
  real_solution = (int *) malloc((myb-myt)*(mzb-mzt) *sizeof(int));
  // Get image
  for(my=myt;my<myb;my++){
    for(mz=mzt;mz<mzb;mz++){
      int b = 2*((mx-ex)*(lcx-ex) + (my-ey)*(lcy-ey) + (mz-ez)*(lcz-ez));
      int a = pow(ex-mx,2) + pow(ey-my,2) + pow(ez-mz,2);
      int c = pow(lcx-ex,2) + pow(lcy-ey,2) + pow(lcz-ez,2) - pow(lr,2);
      real_solution[(mz-mzt)+(my-myt)*(mzb-mzt)] = ((pow(b/2,2) - a*c ) >=0);
    }
  }
  //Print out image in ppm format
  printf("P3\n%d %d\n%d\n",myb-myt,mzb-mzt,maxval+1);
   for(my=myt;my<myb;my++){
    for(mz=mzt;mz<mzb;mz++){
     printf("%d %d %d\n",
	maxval*real_solution[(mz-mzt)+(my-myt)*(mzb-mzt)],
	0,
	maxval*real_solution[(mz-mzt)+(my-myt)*(mzb-mzt)]);
    }
  }
  free(real_solution);
  return 0;
}
