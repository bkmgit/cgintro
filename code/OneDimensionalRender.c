#include "stdio.h"
#include "math.h"
// Compile with gcc OneDimensionalRender.c -lm
int main(){
  const int myt = 1000;   // monitor y location top
  const int myb = 3000;   // monitor y location bottom
  const int mx = 1000;    // monitor x location
  const int ex = 0;       // eye x location
  const int ey = 2000;    // eye y location
  const int lcx = 5000;   // light center x location
  const int lcy = 3000;   // light center y location
  const int lr = 500;     // light radius
  int my;                 // monitor y location
  int real_solution;      // 1 if real solution, 0 otherwise
  for(my=myt;my<myb;my++)
  {
    int b = 2*((ex-mx)*(lcx-ex) + (ey-my)*(lcy-ey));
    int a = pow(ex-mx,2) + pow(ey-my,2);
    int c = pow(lcx-ex,2) + pow(lcy-ey,2) - pow(lr,2);
    real_solution = ((pow(b/2,2) - a*c) >=0 );
    printf("%d %d\n",my,real_solution);
  }
  return 0;
}
