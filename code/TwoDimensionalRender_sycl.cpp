#include<CL/sycl.hpp>
#include <stdio.h>
#include <math.h>
#include <stdlib.h>

/* Compile with 
* 
*  syclcc TwoDimensionalRender_sycl.cpp -lm -o TwoDimensionalRender_sycl
*  run using
*
*  ./TwoDimensionalRender_sycl
*
*  Tested using hipSYCL
*/

int main(){
  const int mx  = 1000;   // monitor x location
  const int myt = 1000;   // monitor y location top
  const int myb = 3000;   // monitor y location bottom
  const int mzt = 1000;   // monitor z location top
  const int mzb = 3000;   // monitor z location bottom
  const int ex  = 0;      // eye x location
  const int ey  = 2000;   // eye y location
  const int ez  = 2000;   // eye z location
  const int lcx = 5000;   // light center x location
  const int lcy = 2000;   // light center y location
  const int lcz = 2000;   // light center z location
  const int lr  = 500;    // light radius
  const int maxval=250;   // maximum rgb color value
  // array to store output, 1 if real solution, 0 otherwise
  int *real_solution;
  real_solution = (int *) malloc((myb-myt)*(mzb-mzt) *sizeof(int));
  // Get image
  // start device queue
  cl::sycl::default_selector deviceSelector;
  cl::sycl::queue queue(deviceSelector);
  // create device memory
  cl::sycl::buffer<int, 1> d_real_solution(real_solution, (myb-myt)*(mzb-mzt));
  queue.submit([&d_real_solution,ex,ey,ez,mx,myb,myt,mzb,lcx,lcy,lcz,lr](
    cl::sycl::handler& cgh) {
    auto real_solution = d_real_solution.get_access
    <cl::sycl::access::mode::write>(cgh);
    cgh.parallel_for<class render>(
      cl::sycl::nd_range<1>{(myb-myt)*(mzb-mzt),1},
      [real_solution,ex,ey,ez,mx,myb,myt,mzb,lcx,lcy,lcz,lr](cl::sycl::nd_item<1> item){
      int global_id = item.get_global_linear_id();
      int kk = global_id % (mzb-mzt);
      int jj = (global_id - kk) / (myb-myt);
      int my = jj + myt;
      int mz = kk + mzt;
      int b = 2*((mx-ex)*(lcx-ex) + (my-ey)*(lcy-ey) + (mz-ez)*(lcz-ez));
      int a = pow(ex-mx,2) + pow(ey-my,2) + pow(ez-mz,2);
      int c = pow(lcx-ex,2) + pow(lcy-ey,2) + pow(lcz-ez,2) - pow(lr,2);
      real_solution[kk + jj*(mzb-mzt)] = ((pow(b/2,2) - a*c ) >= 0); 
    });
  });
  queue.wait();
  //Print out image in ppm format
  printf("P3\n%d %d\n%d\n",myb-myt,mzb-mzt,maxval+1);
  for(int my=myt;my<myb;my++){
    for(int mz=mzt;mz<mzb;mz++){
      printf("%d %d %d\n",
        maxval*real_solution[(mz-mzt)+(my-myt)*(mzb-mzt)],
	0,
	maxval*real_solution[(mz-mzt)+(my-myt)*(mzb-mzt)]);
      }
    }  
  free(real_solution);
  return 0;
}
